package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that will
//be used in handling http request.
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {
	ArrayList<String> enrollees=new ArrayList<>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
//		CaesarEncryptDecrypt("xyz",5);


	}


	//localhost:8080/hello
	@GetMapping("/hello")
	//maps a get request to route /hello
	public String hello(){
		return "<h1>Hello World</h1>";
	}
	//route with string query
	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	//@Request Param annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value="name",defaultValue="John") String name){
		return String.format("Hi %s",name);
	}
	//Multiple parameters
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	//@Request Param annotation that allows us to extract data from query strings in the URL
	public String friend(@RequestParam(value="name",defaultValue="Joe") String name, @RequestParam(value="friend",defaultValue="Juan") String friend){
		return String.format("Hi %s your friend is %s",name,friend);
	}

	//route with path variables
	//dynamic data is obtained directly from the url
	//localhost:8080/{name}
	@GetMapping("/hello/{name}")
	public String greetFriend(@PathVariable("name") String name){

		return String.format("Nce to meet you %s", name);
	}
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user",defaultValue="Joe")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolloing, %s", user);
	}
	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrollees(){

		return enrollees;
	}
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name",defaultValue="Joe")String name,@RequestParam(value="age",defaultValue="18")String age){

		return String.format("Hello %s, my age is, %s", name,age);
	}
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id")String id){
		String message="";
		if(id.equals("java101")){
			message="JAVA 101, MWF 8:00am-11:00am, PHP 3000.00";
		}
		else if(id.equals("sql101")){
			message="SQL 101, TTH 1:00pm-4:00pm, PHP 2000.00";
		}
		else if(id.equals("javaee101")){
			message="JAVA EE 101,MWF 1:00pm-4:00pm, PHP 3500.00";
		}
		else{
			message="course not found";
		}
		return message;
	}
//	public static void CaesarEncryptDecrypt(String inphrase,int key){
//		System.out.print("\n **CAESAR CYPHER**");
//		String phrase=inphrase.toLowerCase();
//		String encrypted="";
//		int i=0;
//		System.out.print("\nyour original phrase:"+phrase);
//		//encryption part
//		for(i=0;i<phrase.length();i++){
//			char x=phrase.charAt(i);
//			int encrpytedpos=(int)((x+(int)key)%'z');
//			if(encrpytedpos<'a'){
//				encrpytedpos+='a'-1;
//			}
//			char letter=(char)encrpytedpos;
//			encrypted+=letter;
//		}
//
//		System.out.print("\nyour encrypted phrase:"+encrypted);
//		//decryption part
//		String decrypted="";
//		for(i=0;i<phrase.length();i++){
//			char x=encrypted.charAt(i);
//			int decrpytedpos=(int)((x-(int)key));
//			if(decrpytedpos<'a'){
//				int distance='a'-decrpytedpos;
//				decrpytedpos='z'-distance+1;
//			}
//			char letter=(char)decrpytedpos;
//			decrypted+=letter;
//		}
//		System.out.print("\nyour decrypted phrase:"+decrypted);
//	}


}
